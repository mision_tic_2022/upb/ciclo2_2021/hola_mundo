public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        //sysout -> tabulador (autocompleta System.out.println())
        System.out.println("Como te extraño python");
        /*
        esto es un 
        bloque de 
        comentario
        */
        //Esto es un comentario de una sola linea
        //declarar una variable
        int numero;
        numero = 10;
        //Declaracion y asignacion de valores
        int num_2 = 5;

        double decimal_double = 5.5;
        float decimal_flotante = (float)5.5;
        boolean bandera = false;
        String cadena = "hola mundo";
        char caracter = 'a';

        if(bandera == true){
            System.out.println("es verdadero");
        }
        
    }
}
